# Build image
docker build -t subhandp/product-out .

# Push image
docker push subhandp/product-out

# Create container
docker container create --name product-out-container subhandp/product-out

# Start container
docker container start product-out-container

# See container logs
docker container logs -f product-out-container

# Stop container
docker container stop product-out-container

# Remove container
docker container rm product-out-container